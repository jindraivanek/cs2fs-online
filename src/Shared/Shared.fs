namespace Shared

type EditorState =
    | Loading
    | Loaded of string
    | ConvertError of string
type Model =
    { Source : string
      FSharpEditorState : EditorState }
    member __.IsLoading =
        __.FSharpEditorState = Loading
    member __.IsError =
        match __.FSharpEditorState with
        | ConvertError _ -> true
        | _ -> false
    member __.Formatted =
        match __.FSharpEditorState with
        | Loading -> ""
        | Loaded code -> code
        | ConvertError err -> err
    static member Default =
                            { Source = ""
                              FSharpEditorState = Loading }


module Route =
    /// Defines how routes are generated on server and mapped from client
    let builder typeName methodName =
        sprintf "/api/cs2fs/%s/%s" typeName methodName

/// A type that specifies the communication protocol between client and server
/// to learn more, read the docs at https://zaid-ajaj.github.io/Fable.Remoting/src/basics.html
type IModelApi =
    {
        init : unit -> Async<Model>
        convert : string -> Async<Result<string, string>>
    }
